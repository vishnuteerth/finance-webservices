package com.java.finance.Database;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class Database {
	String database_url="jdbc:mysql://localhost:3306/finance";
    String username= "root";
    String password="root";
    Connection connection = null;
    
	public Database() {
		try {
	      Class.forName("com.mysql.jdbc.Driver");
	    } catch (ClassNotFoundException e) {
	      System.out.println("ERROR: Unable to load SQLServer JDBC Driver");
	      e.printStackTrace();
	      return;
	    }
	    
	    try {
	      connection = DriverManager.getConnection(database_url, username, password);
	    } catch (SQLException e) {
	      System.out.println("ERROR:  Unable to establish a connection with the database!");
	      e.printStackTrace();
	      return;
	    }
	}
	
	public Connection getConnection() {
		try {
	      connection = DriverManager.getConnection(database_url, username, password);
	    } catch (SQLException e) {
	      System.out.println("ERROR:  Unable to establish a connection with the database!");
	      e.printStackTrace();
	      return null;
	    }
		
		return connection;
	}
	
	public void closeConnection() {
		try {
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}