package com.javacodegeeks.enterprise.rest.jersey;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import com.java.finance.Database.Database;
import com.sun.jersey.core.header.FormDataContentDisposition;

import com.sun.jersey.multipart.FormDataParam;

@Path("/files")
public class JerseyFileUpload {


	@POST
	@Path("/upload")
	@Consumes({MediaType.MULTIPART_FORM_DATA})
	public Response uploadPdfFile(  @FormDataParam("file") InputStream fileInputStream,
	                                @FormDataParam("file") FormDataContentDisposition fileMetaData) throws Exception
	{
	    String UPLOAD_PATH = "/Users/vishnuteerth/Upload/Sales/";
	    try
	    {
	        int read = 0;
	        byte[] bytes = new byte[1024];
	 
	        OutputStream out = new FileOutputStream(new File(UPLOAD_PATH + fileMetaData.getFileName()));
	        while ((read = fileInputStream.read(bytes)) != -1) 
	        {
	            out.write(bytes, 0, read);
	        }
	        out.flush();
	        out.close();
	    } catch (IOException e) 
	    {
	       e.printStackTrace();
	    }
	    return Response.ok("Data uploaded successfully !!").build();
	}

	
	 @POST
	    @Path("add")  
	    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
		@Produces("text/html")
	    public Response addUser(  
	        @FormParam("name") String name,  
	        @FormParam("email") String email,  
	        @FormParam("password") String password)   {  
	    	Database dbs = new Database();
			Connection con = dbs.getConnection();
			PreparedStatement ps = null;
			
			String status = "Successfully Registered";
			
			
			try {
				ps = con.prepareStatement("INSERT INTO registation (name,email,password) VALUES (?,?,?)");
				ps.setString(1, name);
				ps.setString(2, email);
				ps.setString(3, password);
			
				ps.executeUpdate();
			} catch (SQLException e) {
				status = "Registration has failed";
				e.printStackTrace();
			} finally {
				try {
					ps.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				dbs.closeConnection();
			}

			return Response.status(200).entity(status).build();
			
	    }
	    
	  
	    @POST
	    @Path("Login")  
	    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
		@Produces("text/html")
	    public Response Login( 
	    	   		
	        @FormParam("email") String email,  
	        @FormParam("password") String password)  throws URISyntaxException, SQLException  { 
	    	String uri="";
	    	Boolean status=false;
	    	Database dbs = new Database();
			Connection con = dbs.getConnection();
		
			
			try
			{
					PreparedStatement ps=con.prepareStatement("select * from registation where email=? and password=?");  
						ps.setString(1,email);  
						ps.setString(2,password);  
						      
						ResultSet rs=ps.executeQuery();  
						status=rs.next();  
				
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
	    	//if(email.equals("vishnu")&&password.equals("123"))
	    	if(status==true)  
			{
		
					uri=uri+"../Dashboard.jsp";
				}
				else
				{
					uri=uri+"../errorpage.jsp";
				}
					ResponseBuilder response=Response.temporaryRedirect(new URI(uri));
		        return response.build();
			
	    }
	   
	    @POST  
	    @Path("/salesadd")  
	    public Response Salesadd(  
	        @FormParam("Customer") String Customer, @FormParam("invoice") String invoice, @FormParam("transport") String transport,
	        @FormParam("ref") String ref,@FormParam("kindattn") String kindattn ,@FormParam("date") String date,@FormParam("slno") int slno,
	        @FormParam("item") String item,@FormParam("qty") int qty, @FormParam("unitrate") float unitrate,@FormParam("amount") float amount,
	        @FormParam("total") float total, @FormParam("exisepercentage") float exisepercentage, @FormParam("exiseamount") float exiseamount,
	        @FormParam("salestaxper") float salestaxper,@FormParam("salestaxamount") float salestaxamount , @FormParam("roundoff") float roundoff,
	        @FormParam("grandtotal") float grandtotal, @FormParam("cutomertin") String cutomertin ,@FormParam("verifiedby")String verifiedby) {  
	    	Database dbs = new Database();
			Connection con = dbs.getConnection();
			PreparedStatement ps = null;
			
			String status = "Sales Data Successfully Registered!!";
			
			
			try {
				ps = con.prepareStatement("INSERT INTO salesinvoice (Customer,invoice,transport,ref,kindattn,date,slno,item,qty,unitrate,amount,"
						+ "total,exisepercentage,exiseamount,salestaxper,salestaxamount,roundoff,grandtotal,cutomertin,verifiedby) "
						+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
				ps.setString(1, Customer);
				ps.setString(2, invoice);
				ps.setString(3, transport);
				ps.setString(4, ref);
				ps.setString(5, kindattn);
				ps.setString(6, date);
				ps.setInt(7, slno);
				ps.setString(8, item);
				ps.setInt(9, qty);
				ps.setFloat(10, unitrate);
				ps.setFloat(11, amount);
				ps.setFloat(12, total);
				ps.setFloat(13, exisepercentage);
				ps.setFloat(14, exiseamount);
				ps.setFloat(15, salestaxper);
				ps.setFloat(16, salestaxamount);
				ps.setFloat(17, roundoff);
				ps.setFloat(18, grandtotal);
				ps.setString(19,cutomertin);
				ps.setString(20,verifiedby);
				ps.executeUpdate();
			} catch (SQLException e) {
				status = "Sales Entry has failed !! Try Again";
				e.printStackTrace();
			} finally {
				try {
					ps.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				dbs.closeConnection();
			}

			return Response.status(200).entity(status).build();
			  
	    }
	    
	    @POST  
	    @Path("/purchaseadd")  
	    public Response Purchaseadd(  
	    	@FormParam("invno")  String invno,	
	        @FormParam("invdate") String invdate, 
	        @FormParam("orderno") String orderno,
	        @FormParam("orderdate") String orderdate,
	        @FormParam("transport") String transport,
	        @FormParam("date") String date ,
	        @FormParam("payterm") String payterm,
	        @FormParam("slno") int slno,
	        @FormParam("goodsdesc") String goodsdesc,
	        @FormParam("quantity") float quantity,
	        @FormParam("rate") float rate,
	        @FormParam("totamt") float totamt,
	        @FormParam("agform") String agform,
	        @FormParam("vattin") String vattin,
	        @FormParam("csttin") String csttin,
	        @FormParam("pan") String pan,
	        @FormParam("ecc") String ecc
	    		) 
	            
	        {
	    
	    	Database dbs = new Database();
			Connection con = dbs.getConnection();
			PreparedStatement ps = null;
			String status = "Purchase Data Successfully Registered!!";	
			try {
				ps = con.prepareStatement("INSERT INTO purchaseinvoice (invno,invdate,orderno,orderdate,transport,date,payterm,slno,goodsdesc,quantity,rate,"
						+ "totamt,agform,vattin,csttin,pan,ecc) "
						+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
				ps.setString(1, invno);
				ps.setString(2, invdate);
				ps.setString(3, orderno);
				ps.setString(4, orderdate);
				ps.setString(5, transport);
				ps.setString(6, date);
				ps.setString(7, payterm);
				ps.setInt(8, slno);
				ps.setString(9, goodsdesc);
				ps.setFloat(10, quantity);
				ps.setFloat(11, rate);
				ps.setFloat(12, totamt);
				ps.setString(13, agform);
				ps.setString(14, vattin);
				ps.setString(15, csttin);
				ps.setString(16, pan);
				ps.setString(17, ecc);
				ps.executeUpdate();
			} catch (SQLException e) {
				status = "Sales Entry has failed !! Try Again";
				e.printStackTrace();
			} finally {
				try {
					ps.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				dbs.closeConnection();
			}

			return Response.status(200).entity(status).build();
			  
	    }
	   
	    @POST
		@Path("/purchaseupload")
		@Consumes({MediaType.MULTIPART_FORM_DATA})
		public Response purchase(  @FormDataParam("file") InputStream fileInputStream,
		                                @FormDataParam("file") FormDataContentDisposition fileMetaData) throws Exception
		{
		    String UPLOAD_PATH = "/Users/vishnuteerth/Upload/Purchase/";
		    try
		    {
		        int read = 0;
		        byte[] bytes = new byte[1024];
		 
		        OutputStream out = new FileOutputStream(new File(UPLOAD_PATH + fileMetaData.getFileName()));
		        while ((read = fileInputStream.read(bytes)) != -1) 
		        {
		            out.write(bytes, 0, read);
		        }
		        out.flush();
		        out.close();
		    } catch (IOException e) 
		    {
		       e.printStackTrace();
		    }
		    return Response.ok("Data uploaded successfully !!").build();
		}

  } 


