<!-- <html>
<body>
    <h2>Jersey RESTful Web Application!</h2>
    <p><a href="webapi/myresource">Jersey resource</a>
    <p>Visit <a href="http://jersey.java.net">Project Jersey website</a>
    for more information on Jersey!
    <form action="webapi/myresource/add" method="post">  
Enter Id:<input type="text" name="id"/><br/><br/>  
Enter Name:<input type="text" name="name"/><br/><br/>  
Enter Price:<input type="text" name="price"/><br/><br/>  
<input type="submit" value="Add Product"/>  
</form> 
</body>
</html>
-->
<html><head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://pingendo.github.io/pingendo-bootstrap/themes/default/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="css/custom.css" rel="stylesheet" type="text/css">
    <style>
    body {
    padding-top: 90px;
}
.panel-login {
	border-color: #ccc;
	-webkit-box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2);
	-moz-box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2);
	box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2);
}
.panel-login>.panel-heading {
	color: #00415d;
	background-color: #fff;
	border-color: #fff;
	text-align:center;
}
.panel-login>.panel-heading a{
	text-decoration: none;
	color: #666;
	font-weight: bold;
	font-size: 15px;
	-webkit-transition: all 0.1s linear;
	-moz-transition: all 0.1s linear;
	transition: all 0.1s linear;
}
.panel-login>.panel-heading a.active{
	color: #029f5b;
	font-size: 18px;
}
.panel-login>.panel-heading hr{
	margin-top: 10px;
	margin-bottom: 0px;
	clear: both;
	border: 0;
	height: 1px;
	background-image: -webkit-linear-gradient(left,rgba(0, 0, 0, 0),rgba(0, 0, 0, 0.15),rgba(0, 0, 0, 0));
	background-image: -moz-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
	background-image: -ms-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
	background-image: -o-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
}
.panel-login input[type="text"],.panel-login input[type="email"],.panel-login input[type="password"] {
	height: 45px;
	border: 1px solid #ddd;
	font-size: 16px;
	-webkit-transition: all 0.1s linear;
	-moz-transition: all 0.1s linear;
	transition: all 0.1s linear;
}
.panel-login input:hover,
.panel-login input:focus {
	outline:none;
	-webkit-box-shadow: none;
	-moz-box-shadow: none;
	box-shadow: none;
	border-color: #ccc;
}
.btn-login {
	background-color: #59B2E0;
	outline: none;
	color: #fff;
	font-size: 14px;
	height: auto;
	font-weight: normal;
	padding: 14px 0;
	text-transform: uppercase;
	border-color: #59B2E6;
}
.btn-login:hover,
.btn-login:focus {
	color: #fff;
	background-color: #53A3CD;
	border-color: #53A3CD;
}
.forgot-password {
	text-decoration: underline;
	color: #888;
}
.forgot-password:hover,
.forgot-password:focus {
	text-decoration: underline;
	color: #666;
}

.btn-register {
	background-color: #1CB94E;
	outline: none;
	color: #fff;
	font-size: 14px;
	height: auto;
	font-weight: normal;
	padding: 14px 0;
	text-transform: uppercase;
	border-color: #1CB94A;
}
.btn-register:hover,
.btn-register:focus {
	color: #fff;
	background-color: #1CA347;
	border-color: #1CA347;
}

    </style>
   <script type="text/javascript">
      function LoginCheck()
            {
            	var email= document.forms["LoginForm"]["email"].value;
            	var password= document.forms["LoginForm"]["password"].value;
            	var message="Fields Should not be Empty";
            	if(email==null || email=="" )
           		 {
           		document.getElementById("email").focus();
           		document.getElementById("LoginError").innerHTML="Fields Should not be Empty";  
                return false;  
           		
            	}
            	if(password==null || password=="" )
           		 {
           		document.getElementById("password").focus(); 	
           		document.getElementById("LoginError").innerHTML="Fields Should not be Empty";  
                return false;  
           		
            	}
        	}
    </script>
    <script type="text/javascript">
      function RegistrationCheck()
            {
            	var email= document.forms["RegistrationForm"]["email"].value;
            	var password= document.forms["RegistrationForm"]["password"].value;
            	var name= document.forms["RegistrationForm"]["name"].value;
        
            	if(email==null || email=="" )
           		 {
        	   		
        	   		document.getElementById("LoginError").innerHTML="Fields Should not be Empty";  
                	return false;  
           		
            	}
            	if(password==null || password=="" )
        	   		 {
        	   		
        	   		document.getElementById("LoginError").innerHTML="Fields Should not be Empty";  
        	        return false;  
           		
            	}
            	
            	if(name==null || gname=="" )
           		 {
           		 
        	   		document.getElementById("LoginError").innerHTML="Fields Should not be Empty";  
        	        return false;  
        	   		
            	}
            	
        	}
    </script>
    <script type="text/javascript">
      function PasswordRecoveryValidate()
            { 
            	var RecoveryEmail= document.getElementById("RecoveryEmail").value;
            	var RecoveryName=document.getElementById("RecoveryName").value;
        
            	if(RecoveryEmail==null || RecoveryEmail=="")
            	{
            		document.getElementById("RecoveryEmail").focus();
            		alert("Email Adress for Password Recovery Cannot be blank");
            		//document.getElementById("RecoveryError").innerHTML="Email Address Cannot be blank";  
            		return false; 
        
            	}
            	if(RecoveryName==null || RecoveryName=="" )
           		 {
        	   		document.getElementById("RecoveryName").focus();
        	   		//document.getElementById("RecoveryError").innerHTML="Name Should not be Empty"; 
        	   		alert("Name for Password Recovery Cannot BE blank"); 
                	return false;  
           		
            	}
            	
        	}
    </script> 
  </head><body>
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-md-offset-3">
          <div class="panel panel-login">
            <div class="panel-heading"><span id="LoginError"></span>
              <div class="row">
                <div class="col-xs-6">
                  <a href="#" class="active" id="login-form-link">Login</a>
                </div>
                <div class="col-xs-6">
                  <a href="#" id="register-form-link">Register</a>
                </div>
              </div>
              <hr>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-lg-12">
                  
                  <form id="login-form" action="rest/files/Login" method="post" role="form" name="LoginForm" onsubmit="return LoginCheck()" style="display: block;">
		
                    <div class="form-group">
                      <input type="email" required name="email" id="email" pattern="[A-Za-z0-9._%+-]{3,}@[a-zA-Z]{3,}([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})" tabindex="1" class="form-control" placeholder="Email Address" value="">
                    </div>
                    <div class="form-group">
                      <input type="password" required name="password" id="password"  tabindex="2" class="form-control" placeholder="Password">
                    </div>
                    
                    <div class="form-group">
                      <div class="row">
                        <div class="col-sm-6 col-sm-offset-3">
                          <input type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-login" value="Log In">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-lg-12">
                          <div class="text-center">
                            <a href="#PasswordRecovery" tabindex="5" class="forgot-password" data-toggle="modal">Forgot Password?</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </form>
                  <form id="register-form"  action="rest/files/add" onsubmit="return RegistrationCheck()" name="RegistrationForm" method="post" role="form" style="display: none;">
                    <div class="form-group">
                      <input type="text" required name="name"  id="name" tabindex="1" class="form-control" placeholder="Name" value="">
                    </div>
                    <div class="form-group"><span id="isE"></span> 
                     <input type="email" required name="email" pattern="[A-Za-z0-9._%+-]{3,}@[a-zA-Z]{3,}([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})" id="Regemail" tabindex="1" class="form-control email" placeholder="Email Address" value=""><span class="status"></span>  

                    </div>
                    <div class="form-group">
                      <input type="password" required name="password" id="Regpassword" tabindex="2" class="form-control" placeholder="Password">
                    </div>
                    
                    <div class="form-group">
                      <div class="row">
                        <div class="col-sm-6 col-sm-offset-3">
                          <input type="submit" name="register-submit" id="register-submit" tabindex="4" class="form-control btn btn-register" value="Register Now">
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  
                 
     
      
    <script>
      $(function() {
            
                $('#login-form-link').click(function(e) {
            		$("#login-form").delay(100).fadeIn(100);
             		$("#register-form").fadeOut(100);
            		$('#register-form-link').removeClass('active');
            		$(this).addClass('active');
            		e.preventDefault();
            	});
            	$('#register-form-link').click(function(e) {
            		$("#register-form").delay(100).fadeIn(100);
             		$("#login-form").fadeOut(100);
            		$('#login-form-link').removeClass('active');
            		$(this).addClass('active');
            		e.preventDefault();
            	});
            
            });
    </script>
    <div class="modal fade" id="PasswordRecovery">
    <span id="RecoveryError"></span>
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
            <h4 class="modal-title">Password Recovery</h4>
          </div>
          <div class="modal-body">
            <form role="form" method="post" onsubmit="return PasswordRecoveryValidate()">
              <div class="form-group">
                <label class="control-label" for="exampleInputEmail1">Email Address</label>
                <input class="form-control" id="RecoveryEmail" name="RecoveryEmail" placeholder="Enter email" type="email" required>
              </div>
              <div class="form-group">
                <label class="control-label" for="name">Name</label>
                <input class="form-control" id="RecoveryName" name="RecoveryName" placeholder="Name" type="text" required>
              </div>
              <button type="submit" class="btn btn-primary">Get Password</button>
            </form>
          </div>
          <div class="modal-footer">
            <a class="btn btn-default" data-dismiss="modal">Close</a>
          </div>
        </div>
      </div>
      </div>
 
<script>
<script type="text/javascript">  
$(document).ready(function(){  
    $(".email").change(function(){  
        var email = $(this).val();  
        if(email.length >= 3){  
            $(".status").html("<font color=gray> Checking availability...</font>");  
             $.ajax({  
                type: "POST",  
                url: "CheckAvaliablity.jsp",  
                data: "email="+ email,  
                success: function(msg){  

                    $(".status").ajaxComplete(function(event, request, settings){  
                           
                        $(".status").html(msg);  

                    });  
                }  
            });   
        }  
        else{  
               
            $(".status").html("<font color=red>email should be <b>3</b> character long.</font>");  
        }  
          
    });  
});  
</script>  
   
  


</body></html>